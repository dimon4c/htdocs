<!--HEADER-->
<header class="l-header l-header-1 t-header-1">
    <div class="navbar navbar-ason">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#ason-navbar-collapse"
                        class="navbar-toggle collapsed"><span class="sr-only">Navigation</span><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <!--  <a href="<?php echo SITE_INDEX; ?>" class="navbar-brand widget-logo"><span class="logo-default-header"><img
                            src="img/logo_dark.png" alt=""></span></a>-->
            </div>
            <div id="ason-navbar-collapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php if (isAuth()) {
                        echo '  <li class="lock"><a href="logout.php"><i class="fa fa-lock"></i>Log Out</a></li>';
                    } else {
                        echo '  <li class="lock"><a href="login.php"><i class="fa fa-lock"></i>Log In</a></li>';
                    } ?>
                </ul>
            </div>
            <div class="container-fluid">
                <img src="source/usm-presentation1.jpg" style="width: 32.33%">
                <img src="source/usm-presentation3.jpg" style="width: 32.33%">
                <img src="source/usm-presentation2.jpg" style="width: 32.33%">
            </div>
        </div>
    </div>
</header>
