<body>

<!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!--THEME SETTINGS-->
<div class="widget-theme-settings"><a href="#" class="theme-settings-toggle"><i class="fa fa-cog"></i></a>

    <div class="theme-settings-wrapper">
        <h3>Theme Settings</h3>

        <div class="hide switchery-success"></div>
        <div class="hide switchery-info"></div>
        <div class="hide switchery-warning"></div>
        <div class="theme-settings-row">
            <h4 class="theme-skin-title">Base Font Size</h4>
            <input id="spinnerBaseFont" type="text" value="14" name="spinnerBaseFont" placeholder="Html base font"
                   title="REM based font sizing." class="tt-theme-settings">
        </div>
        <hr>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label for="boxedLayout">Boxed Layout</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input">
                <input id="boxedLayout" type="checkbox" class="switcheryBoxed">
            </div>
        </div>
        <hr>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label for="fixedHeader">Fixed Header</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input">
                <input id="fixedHeader" type="checkbox" class="switcheryFixedHeader">
            </div>
        </div>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label for="stickyHeader">Sticky Header</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input">
                <input id="stickyHeader" type="checkbox" class="switcheryStickyHeader">
            </div>
        </div>
        <hr>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label for="fixedFooter">Fixed Footer</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input">
                <input id="fixedFooter" type="checkbox" class="switcheryFixedFooter">
            </div>
        </div>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label for="stickyFooter">Sticky Footer</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input">
                <input id="stickyFooter" type="checkbox" class="switcheryStickyFooter">
            </div>
        </div>
        <hr>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label>Main Menu</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input"><a href="layout-menu.html" data-placement="left"
                                                             title="For more Menu Options check out the documentation."
                                                             class="theme-settings-info tt-theme-settings"><i
                        class="fa fa-info"></i></a></div>
        </div>
        <hr>
        <div class="l-row theme-settings-row">
            <div class="l-span-xs-7">
                <label>Sidebar</label>
            </div>
            <div class="l-span-xs-5 theme-settings-input"><a href="layout-sidebar-left.html" data-placement="left"
                                                             title="For more Sidebar Options check out the 'Layouts -&gt; Sidebar' section."
                                                             class="theme-settings-info tt-theme-settings"><i
                        class="fa fa-info"></i></a></div>
        </div>
        <hr>
    </div>
</div>
<!--SECTION-->
<section class="l-main-container">
    <!--Left Sidebar Content-->
    <aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
        <!--Switcher-->
        <div class="l-side-box"><a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar"
                                   data-ason-target="#sb-left"
                                   class="sidebar-switcher switcher t-switcher-side ason-widget"><i
                    class="fa fa-bars"></i></a></div>

        <!--Main Menu-->
        <div class="l-side-box">
            <!--MAIN NAVIGATION MENU-->
            <nav class="navigation">
                <div style="margin-left: auto;margin-right: auto;">
                    <img src="../source/usm-logo.png" style="width: 50%;">
                </div>
                <ul data-ason-type="menu" class="ason-widget">

                    <li><a href="index.php"><i class="icon fa fa-home"></i><span
                                class="title">View CV's</span></a>
                    </li>
                    <?php if (isAuth()) { ?>
                        <li><a href="personalPage.php"><i class="icon fa fa-user"></i><span
                                    class="title">Personal Page</span></a>
                        </li>
                    <?php } ?>
                    <?php if (isAdmin()) { ?>
                        <li>
                            <a href="user.php">
                                <i class="icon fa fa-users"></i><span class="title">Users</span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </aside>
