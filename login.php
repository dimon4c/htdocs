<?php
include_once('includes/head.php');

?>
<body class="login-bg">

<!--[if lt IE 9]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!--SECTION-->
<section class="l-main-container">
    <div class="container-fluid">
        <img src="source/usm-presentation1.jpg" style="width: 32.33%">
        <img src="source/usm-presentation3.jpg" style="width: 32.33%">
        <img src="source/usm-presentation2.jpg" style="width: 32.33%">
    </div>
    <!--Main Content-->
    <div class="login-wrapper">
        <div class="login-container">

            <!--Login Form-->
            <form id="loginForm" method="post" role="form" action="data/login.php" class="login-form">
                <div class="form-group">
                    <input id="login" type="text" name="login" placeholder="Login" class="form-control">
                </div>
                <div class="form-group">
                    <input id="password" type="password" name="password" placeholder="Password" class="form-control">
                </div>
                <div class="container-fluid" style="padding: 0;">
                    <div class="col-md-6" style="padding: 0;">
                        <button type="submit" class="btn btn-dark btn-block  btn-login">Sign
                            In
                        </button>
                    </div>
                    <div class="col-md-6" style=" padding: 0;">
                        <button class="btn btn-primary btn-block btn-login "
                                onclick="<?php SITE_INDEX ?>">
                            Home
                        </button>
                    </div>
                </div>
                <div class="login-social">
                    <div class="l-span-md-12">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- ===== JS =====-->
<!-- jQuery-->
<script src="js/basic/jquery.min.js"></script>
<script src="js/basic/jquery-migrate.min.js"></script>
<!-- General-->
<script src="js/basic/modernizr.min.js"></script>
<script src="js/basic/bootstrap.min.js"></script>
<script src="js/shared/jquery.asonWidget.js"></script>
<script src="js/plugins/plugins.js"></script>
<script src="js/general.js"></script>
<!-- Semi general-->
<script type="text/javascript">
    var paceSemiGeneral = {restartOnPushState: false};
    if (typeof paceSpecific != 'undefined') {
        var paceOptions = $.extend({}, paceSemiGeneral, paceSpecific);
        paceOptions = paceOptions;
    } else {
        paceOptions = paceSemiGeneral;
    }

</script>
<script src="js/plugins/pageprogressbar/pace.min.js"></script>
<!-- Specific-->
<script src="js/plugins/forms/validation/jquery.validate.min.js"></script>
<script src="js/plugins/forms/validation/jquery.validate.additional.min.js"></script>
<script src="js/calls/page.login.js"></script>
</body>

<!-- Mirrored from themes.loxdesign.net/proteus/themes/admin/default/theme/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Jun 2015 13:09:57 GMT -->
</html>