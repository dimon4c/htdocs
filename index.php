<?php
include_once('data/functions.php');
include_once('variables.php');

include_once('includes/head.php');
include_once('includes/menu.php');
?>
<!--Main Content-->
<section class="l-container">
    <?php include_once('includes/topHeader.php') ?>
    <div class="l-page-header">
        <h2 class="l-page-title"><span>All CV's</span></h2>
        <!--BREADCRUMB-->
        <ul class="breadcrumb t-breadcrumb-page">
            <li><a href="index.php">All cv's</a></li
        </ul>

    </div>
    <div class="l-spaced">
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">
                    <button id="refresh" onClick="location.reload();"
                            class="m-10 btn btn-primary btn-lg btn-eff btn-eff-2"
                            type="button"
                            data-toggle="tooltip" title="Refresh page" data-original-title="Refresh page"><i
                            class="fa fa-refresh"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="l-spaced">
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">
                    <table id="dataTableId" cellspacing="0" width="100%" class="display">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>CV</th>
                            <th>Faculty</th>
                            <th>Date Upload</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>CV</th>
                            <th>Faculty</th>
                            <th>Date Upload</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        <?php include_once('data/functions.php');
                        getAllCV(); ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ===== JS =====-->

<!-- jQuery-->
<script src="js/basic/jquery.min.js"></script>
<script src="js/basic/jquery-migrate.min.js"></script>
<!-- General-->
<script src="js/basic/modernizr.min.js"></script>
<script src="js/basic/bootstrap.min.js"></script>
<script src="js/shared/jquery.asonWidget.js"></script>
<script src="js/plugins/plugins.js"></script>
<script src="js/general.js"></script>
<!-- Semi general-->
<script type="text/javascript">
    var paceSemiGeneral = {restartOnPushState: false};
    if (typeof paceSpecific != 'undefined') {
        var paceOptions = $.extend({}, paceSemiGeneral, paceSpecific);
        paceOptions = paceOptions;
    } else {
        paceOptions = paceSemiGeneral;
    }

</script>
<script src="js/plugins/pageprogressbar/pace.min.js"></script>
<!-- Specific-->
<script src="js/shared/classie.js"></script>
<script src="js/shared/perfect-scrollbar.min.js"></script>
<script src="js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
<script src="js/plugins/forms/elements/jquery.checkBo.min.js"></script>
<script src="js/plugins/forms/elements/jquery.switchery.min.js"></script>
<script src="js/plugins/table/jquery.dataTables.min.js"></script>
<script src="js/plugins/tooltip/jquery.tooltipster.min.js"></script>
<script src="js/calls/part.header.1.js"></script>
<script src="js/calls/part.sidebar.2.js"></script>
<script src="js/calls/part.theme.setting.js"></script>
<script src="js/calls/table.data.js"></script>
<script src="js/plugins/forms/elements/jquery.checkradios.min.js"></script>
<script src="js/plugins/forms/upload/jquery.ui.widget.js"></script>
<script src="js/plugins/forms/upload/jquery.tmpl.min.js"></script>
<script src="js/plugins/forms/upload/jquery.load-image.all.min.js"></script>
<script src="js/plugins/forms/upload/jquery.canvas-to-blob.min.js"></script>
<script src="js/plugins/forms/upload/jquery.blueimp-gallery.min.js"></script>
<script src="js/plugins/forms/upload/jquery.iframe-transport.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-process.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-image.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-audio.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-video.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-validate.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-ui.js"></script>
<script src="js/plugins/forms/upload/jquery.summernote.min.js"></script>
<script src="js/plugins/forms/elements/jquery.switchery.min.js"></script>
<script src="js/plugins/tooltip/jquery.tooltipster.min.js"></script>
<script src="js/calls/form.upload.js"></script>

</body>

</html>
