<?php

include_once('data/functions.php');
include_once('variables.php');

if (!isAuth()) {

    header('Location: ' . SITE_INDEX_LOGIN_PAGE . "dff");

}
include_once('includes/head.php');
include_once('includes/menu.php');


?>

<!--Main Content-->
<section class="l-container">
    <?php include_once('includes/topHeader.php') ?>
    <div class="l-page-header">
        <h2 class="l-page-title"><span>Upload CV</span></h2>
        <!--BREADCRUMB-->
        <ul class="breadcrumb t-breadcrumb-page">
            <li><a href="index.php">Home</a></li>
            <li class="active">Upload CV</li>
        </ul>
    </div>
    <div class="l-spaced">
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">

                    <button id="refresh" onClick="location.reload();"
                            class="m-10 btn btn-primary btn-lg btn-eff btn-eff-2"
                            type="button"
                            data-toggle="tooltip" title="Refresh page " data-original-title="Refresh page"><i
                            class="fa fa-refresh "></i></button>

                </div>
            </div>
        </div>
    </div>
    <div class="l-spaced">
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">
                    <form name="idForm" id="idForm" enctype="multipart/form-data" value="uploadReports">
                        <input type="hidden" id="action" name="action" value="uploadReports">

                        <div class="form-group"><label for="basicText" class="col-sm-3 control-label"> Choose CV's for upload
                            </label> <input type="file" multiple name="imgFileToUpload[]" id="imgFileToUpload"
                                            class="form-control" accept="*/*"></div>
                    </form>
                    <button type="button" onclick="save()" class="btn btn-default"
                            data-dismiss="modal">Ok
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="l-spaced">
        <div class="l-row">
            <h2 class="l-page-title"><span>CV's</span></h2>
        </div>
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">
                    <table id="dataTableId" cellspacing="0" width="100%" class="display">
                        <thead>
                        <tr>
                            <th>CV</th>
                            <th>Faculty</th>
                            <th>Date Upload</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>CV</th>
                            <th>Faculty</th>
                            <th>Date Upload</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        <?php include_once('data/functions.php');
                        getAllCVs(); ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<!--Modal Window -->
<div class="modal fade" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title"> Message </h4>
            </div>
            <div id="message-body" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
        <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
</div>
<!-- /.modal-->
<!-- ===== JS =====-->
<!-- jQuery-->
<script src="js/basic/jquery.min.js"></script>
<script src="js/basic/jquery-migrate.min.js"></script>
<!-- General-->
<script src="js/basic/modernizr.min.js"></script>
<script src="js/basic/bootstrap.min.js"></script>
<script src="js/shared/jquery.asonWidget.js"></script>
<script src="js/plugins/plugins.js"></script>
<script src="js/general.js"></script>
<!-- Semi general-->
<script type="text/javascript">
    var paceSemiGeneral = {restartOnPushState: false};
    if (typeof paceSpecific != 'undefined') {
        var paceOptions = $.extend({}, paceSemiGeneral, paceSpecific);
        paceOptions = paceOptions;
    } else {
        paceOptions = paceSemiGeneral;
    }

    function save() {

        var form = document.forms.namedItem("idForm");
        var oOutput = "";
        oData = new FormData(form);

        var oReq = new XMLHttpRequest();
        oReq.open("POST", "data/hackatonFunctions.php", true);
        oReq.addEventListener("load", transferComplete, false);
        oReq.onload = function (oEvent) {
            if (oReq.status == 200) {
                oOutput = "Uploaded!   <br>" + oReq.responseText;
            } else {
                oOutput = "Error " + oReq.status + " occurred when trying to upload your file.<br \/>";
            }
            $('#message-body').html(oOutput);
        };
        oReq.send(oData);
    }
    function transferComplete(evt) {
        $('#messageModal').modal('show');
    }

</script>
<script src="js/plugins/pageprogressbar/pace.min.js"></script>
<!-- Specific-->
<script src="js/shared/classie.js"></script>
<script src="js/shared/jquery.cookie.min.js"></script>
<script src="js/shared/perfect-scrollbar.min.js"></script>
<script src="js/plugins/accordions/jquery.collapsible.min.js"></script>
<script src="js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
<script src="js/plugins/forms/elements/jquery.checkBo.min.js"></script>
<script src="js/plugins/forms/elements/jquery.checkradios.min.js"></script>
<script src="js/plugins/forms/upload/jquery.ui.widget.js"></script>
<script src="js/plugins/forms/upload/jquery.tmpl.min.js"></script>
<script src="js/plugins/forms/upload/jquery.load-image.all.min.js"></script>
<script src="js/plugins/forms/upload/jquery.canvas-to-blob.min.js"></script>
<script src="js/plugins/forms/upload/jquery.blueimp-gallery.min.js"></script>
<script src="js/plugins/forms/upload/jquery.iframe-transport.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-process.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-image.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-audio.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-video.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-validate.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-ui.js"></script>
<script src="js/plugins/forms/upload/jquery.summernote.min.js"></script>
<script src="js/plugins/forms/elements/jquery.switchery.min.js"></script>
<script src="js/plugins/tooltip/jquery.tooltipster.min.js"></script>
<script src="js/calls/form.upload.js"></script>
<script src="js/calls/part.header.1.js"></script>
<script src="js/calls/part.sidebar.2.js"></script>
<script src="js/calls/part.theme.setting.js"></script>


<script type="text/javascript">
    function openWindow() {
        $('#successModal').modal('show');
    }
</script>
</body>
</html>
