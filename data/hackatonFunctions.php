<?php
include_once("../variables.php");

if (isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch ($action) {
        case 'uploadReports' :
            echo upload($_FILES, "imgFileToUpload", '../documents/works/', '../documents/repository/');
            break;


        default:
            echo "Some thing it is wrong";
            break;
    }

}


class DB
{
    protected static $instance = null;

    public function __construct()
    {
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function instance()
    {
        if (self::$instance === null) {
            $opt = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => TRUE,
            );
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '; ';
            self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
        }
        return self::$instance;
    }

    public static function run($sql, $args = [])
    {
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    public function __clone()
    {
    }
}

function reArrayFiles(&$file_post)
{

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}


function upload($params, $id, $destination, $repository)
{
    $out = "";
    // Count # of uploaded files in array
    $total = count($params[$id]['name']);

// Loop through each file
    for ($i = 0; $i < $total; $i++) {
        //Get the temp file path
        $tmpFilePath = $params[$id]['tmp_name'][$i];
        //Make sure we have a filepath
        if ($tmpFilePath != "") {
            //Setup our new file path
            $fileName = $params[$id]['name'][$i];
            $newFilePath = $destination . $fileName;

            $ext = explode('.', $fileName);
            $extension = $ext[(count($ext) - 1)];
            $name = "";
            for ($j = 0; $j < (count($ext) - 1); $j++) {
                $name .= $ext[$j];
            }
            $newname = $name . '-' . time() . '-' . rand();
            $outFileName = $newname . "." . $extension;
            $full_local_path = $destination . $outFileName;
            $repository_local_path = $repository . $outFileName;

            copy($tmpFilePath, $repository_local_path);
            chmod($repository_local_path, 0777);
            if (move_uploaded_file($tmpFilePath, $full_local_path)) {

                if (!isset($_SESSION)) {
                    session_start();
                }
                $userId = $_SESSION['userId'];
                $stmt = DB::prepare("INSERT INTO `uploads` (`id_user`, `fileName` , `date`) VALUES ('$userId', '$outFileName',NOW())");
                $stmt->execute();

                $out .= $outFileName . "<br>";
            }
        }
    }


    return $out;
}


/*
 * Function for lever acces
 *
 */

function  isAuth()
{

    $login = $_SESSION['login'];
    $hash = $_SESSION['hash'];

    if (isset($login) && isset($hash) && !empty($login) && !empty($hash)) {
        $row = DB::run("SELECT  login FROM admintable WHERE pass=? AND login=?", [$hash, $login])->fetchColumn();
        if (strcmp($_SESSION['login'], $row) == 0) {
            return true;
        }
    }
    return false;
}

function  isAdmin()
{
    if (isAuth()) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $hash = $_SESSION['hash'];
        $login = $_SESSION['login'];
        $row = DB::run("SELECT  isAdmin FROM admintable WHERE pass=? AND login = ?", [$hash, $login])->fetchColumn();
        if ((strcmp("1", $row) == 0)) {
            return true;
        }
    }

    return false;
}




