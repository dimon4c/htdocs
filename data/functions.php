<?php
include("variables.php");

if (isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch ($action) {

        case 'updateUser' :
            echo updateUser($_POST);
            break;

        case 'editUser' :
            getEditUserForm($_POST);
            break;

        case 'deleteUser' :
            echo deleteUserForm($_POST);
            break;

        case 'addUser' :
            userForm();
            break;

        default:
            echo "Some thing it is wrong";
            break;
    }
}

class DB
{
    protected static $instance = null;

    public function __construct()
    {
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function instance()
    {
        if (self::$instance === null) {
            $opt = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => TRUE,
            );
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '; ';
//            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHAR;
            self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
        }
        return self::$instance;
    }

    public static function run($sql, $args = [])
    {
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    public function __clone()
    {
    }
}

function saveOrder($params)
{
    $id = $params['id'];
    $surname = $params['surname'];
    $address = $params['address'];
    $phone = $params['phone'];
    $delivery = $params['delivery'];
    $noteOrder = $params['notice'];
    $total = $params['total'];
    $row = DB::run("UPDATE orders SET   cust_name = ? , cust_adress = ?, cust_phone= ?, delivery_cost =?,notice=?,total=?  WHERE id=?", [$surname, $address, $phone, $delivery, $noteOrder, $total, $id]);

    $items = $params["items"];
    foreach ($items as $key => $value) {

        $idd = $items [$key];

        if (!empty($idd)) {

            $row = DB::run("UPDATE cart_items SET   quantity=?  WHERE id=?", [$value, $key]);
        }

    }


}

function processOrder($params)
{
    $id = intval($params['processOrder']);

    $row = DB::run("SELECT  status  FROM orders WHERE id=?", [$id])->fetchColumn();
    if ($row == 1) {
        $row = DB::run("UPDATE orders SET   status = '2'    WHERE id=?", [$id]);

        if ($row->rowCount() == 1) {
            return "Comanda a trecut la etapa de procesare.";
        }
        return "Va rugam sa mai incercati o data.";
    } elseif ($row == 2) {
        return "Va rugam sa alegeti alta comanda deoarece ea deja se proceseaza.";
    } elseif ($row == 3) {
        return "Comanda a trecut la etapa de procesare.";
    } elseif ($row == 4) {
        return "Comanda a trecut la etapa de procesare.";
    }
}

function completeOrder($params)
{
    $id = intval($params['completeOrder']);

    $row = DB::run("SELECT  status  FROM orders WHERE id=?", [$id])->fetchColumn();
    if ($row == 1) {
        return "Comanda nu poate fi completata deoarece nu a fost procesata";
    } elseif ($row == 2) {
        $row = DB::run("UPDATE orders SET   status = '3'    WHERE id=?", [$id]);

        if ($row->rowCount() == 1) {
            return "Comanda este completata ";
        }
        return "Va rugam sa mai incercati o data.";
    } elseif ($row == 3) {
        return "Va rugam sa alegeti alta comanda deoarece ea deja completata.";
    } elseif ($row == 4) {
        return "Va rugam sa alegeti alta comanda deoarece ea deja inchisa.\n
                 sau sa o treceti la etapa de procesare pentru a putea fi completata";
    }

}

function closeOrder($params)
{
    $id = intval($params['closeOrder']);
    $row = DB::run("UPDATE orders SET   status = '4'    WHERE id=?", [$id]);

    if ($row->rowCount() == 1) {
        return "Comanda a fost inchisa.";
    }
    return "Va rugam sa mai incercati o data.";

}

function removeItem($params)
{
    $id = intval($params['removeItem']);
    $row = DB::run("DELETE  FROM cart_items WHERE id=?", [$id]);
    return $row->rowCount();
}

function editOrderForm($params)
{
    $isAdmin = isAdmin();

    $id = $params['idOrder'];
    $row = DB::run("SELECT * FROM orders WHERE id=?", [$id])->fetch();

    echo '<form id="idForm" name="idForm"    >' .
        '<input type = "hidden" id= "action" name= "action" value="updateOrder">' .
        '<input type = "hidden" id= "idOrder" name= "idOrder"  value="' . $id . '">';

    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Nume Prenume</label>   <div class="col-sm-9"> <input type = "text" id= "nameOrder" name= "nameOrder"      class="form-control"  ';
    if (!$isAdmin) echo "disabled";
    echo '  value="' . $row['cust_name'] . '"></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Adresa</label>   <div class="col-sm-9"> <input type = "text" id= "addresOrder" name= "addresOrder"  class="form-control" ';
    if (!$isAdmin) echo "disabled";
    echo '  value="' . $row['cust_adress'] . ' "></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Telefon</label>   <div class="col-sm-9"> <input type = "text" id= "phoneOrder" name= "phoneOrder"  class="form-control" ';
    if (!$isAdmin) echo "disabled";
    echo '  value="' . $row['cust_phone'] . ' "></div></div>';


    echo '<div class="form-group">
<label for="basicText" class="col-sm-3 control-label">Tipul de trimitere</label>
<div class="col-sm-9">
<select name="deliveryOrder" id="deliveryOrder" class="form-control" ';
    if (!$isAdmin) echo "disabled";
    echo '>
 <option value="35" ';
    if ($row['delivery_cost'] == 35) echo "selected";
    echo '  >Expediere normala 35 lei</option>
 <option value="50" ';
    if ($row['delivery_cost'] == 50) echo "selected";
    echo '>Expediere express 50 lei</option>
</select>

</div>
</div>';


    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Total</label>   <div class="col-sm-9"> <input type = "text" id= "priceProduct" name= "paymentOrder"    class="form-control" ';
    if (!$isAdmin) echo "disabled";
    echo '  value=" ' . $row['total'] . '"></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Note</label>   <div class="col-sm-9"> <textarea id="noteOrder" name="noteOrder" rows="3" placeholder="Textarea" class="form-control"  ';
    if (!$isAdmin) echo "disabled";
    echo ' ">' . $row['notice'] . '</textarea></div></div>';

    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Produse</label>   <div class="col-sm-9">';
    echo '<div class="form-group"><table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th  colspan="2">Detalii  Product</th>
                      <th>Cantitatea</th>
                      <th>Pretul </th>           
                      <th>Suma</th>
                      <th>Optiuni</th>
                    </tr>
                  </thead>
                  <tbody>';
    $stmt2 = DB::run("SELECT cart_items.id AS id, quantity,cart_items.price AS price ,code, ro_name FROM cart_items,products WHERE products.id=cart_items.product_id AND  order_id=?", [$id]);
    $i = 0;

    $total = 0;
    while ($row2 = $stmt2->fetch(PDO::FETCH_LAZY)) {

        $idItem = $row2['id'];
        ++$i;
        echo '<tr id="item' . $idItem . '">';
        echo '<td>' . $i . '</td>';
        echo '<td>' . $row2['code'] . '  </td> <td> ' . $row2['ro_name'] . ' </td>';
        echo '<td> ';
        echo '  <table> <tr>
                 <td><button  ';
        if (!$isAdmin) echo "disabled";
        echo '   onclick="decr(' . $idItem . ');" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="" data-original-title=""><div class="icon-hover"><i class="fa fa-minus"></i> </div></button></td>';
        echo '  <td> <input  ';
        if (!$isAdmin) echo "disabled";
        echo '  id="order' . $idItem . '" type="text"  value="' . $row2['quantity'] . '"  class="form-control" style="display: block;"> </td>';
        echo ' <td>     <button   ';
        if (!$isAdmin) echo "disabled";
        echo '  onclick="incr(' . $idItem . ');" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="" data-original-title=""><div class="icon-hover"><i class="fa fa-plus"></i></div></button></td></tr></table></td>';
        $price = $row2['price'];
        echo '<td id="price' . $idItem . '" name="price" >' . $price . ' </td>';
        $sum = $price * $row2['quantity'];
        echo '<td id="sum' . $idItem . '" name = "kerf">' . $sum . ' </td>';
        echo '<td>    <button  ';
        if (!$isAdmin) echo "disabled";
        echo '  id="remo' . $idItem . '"  onClick="rem(' . $idItem . ');" class="btn btn-sm btn-default" type="button" data-toggle="tooltip" ><div class="icon-hover"><i class="fa fa-times"></i> </div></button> </td> ';
        echo '</tr>';

        $total += $sum;
    }

    echo '<tr><td></td>  <td></td><td></td> <td></td><td>Total:</td><td id="total">' . $total . '</td><td></td></tr>';
    echo '</tbody></table> </div></form>';

    echo '<button onclick="processOrder(' . $id . ')" type="button" class="btn btn-dark btn-lg  active width-30 mr">Proceseaza</button>';
    if ((!isAdmin() && isProcessing($row['status'])) || isAdmin()) {
        echo '<button onclick="completeOrder(' . $id . ')"  type="button" class="btn btn-success btn-lg active width-30 mr">Completa</button>';
    }
    if (isAdmin()) {
        echo '<button onclick="closeOrder(' . $id . ')"  type="button" class="btn btn-danger btn-lg active width-30 ">Inchide</button>';
    }
}

function isProcessing($status)
{

    if ($status == 2) {
        return true;
    }

    return false;
}

function getStatusName($id)
{
    if ($id == 1) {
        return '  <td data-value="1"><span title="New" class="label label-success">New</span></td>';
    } elseif ($id == 2) {
        return '  <td data-value="2"><span title="Processing" class="label label-dark">Processing</span></td>';

    } elseif ($id == 3) {
        return '  <td data-value="3"><span title="Finished" class="label label-success">Finished</span></td>';

    } elseif ($id == 4) {
        return '  <td data-value="4"><span title="Closed" class="label label-danger">Closed</span></td>';

    }
    return "";
}

function deleteOrderForm($params)
{
    $id = intval($params['idOrder']);
    $row = DB::run("DELETE  FROM cart_items WHERE order_id=?", [$id]);
    if ($row->rowCount() > 0) {
        $row = DB::run("DELETE  FROM orders WHERE id=?", [$id]);
    }
    return $row->rowCount();
}

function getAllCategoriesMenu()
{

}

function getAllCV()
{
    $whereCondition = "";

    $stmt = DB::run("SELECT * FROM uploads, admintable  WHERE uploads.id_user = admintable.id " . $whereCondition . "order by uploads.date desc");
    while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {

        echo "<tr  id='row" . $row['id'] . "'>";
        echo '<td>' . $row['name'] . '</td>';
        echo '<td>' . $row['surname'] . '</td>';
        echo '<td> <a target="_blank" href="' . DIRECTORY_REPOSITORY_FILE_STORE . '/' . $row['fileName'] . '" >' . $row['fileName'] . ' </a> </td>';
        echo '<td> ' . $row['faculty'] . '  </td>';
        echo '<td>' . $row['date'] . '</td>';
        echo "</tr>";

    }

}


function getAllCVs()
{
    $whereCondition = "";
    $whereCondition = " and  admintable.login= ? ";
    $stmt = DB::run("SELECT * FROM uploads, admintable  WHERE uploads.id_user = admintable.id " . $whereCondition . "order by uploads.date desc", [$_SESSION["login"]]);
    while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {

        echo "<tr  id='row" . $row['id'] . "'>";
        echo '<td> <a target="_blank href="' . DIRECTORY_REPOSITORY_FILE_STORE . '/' . $row['fileName'] . '" >' . $row['fileName'] . ' </a> </td>';
        echo '<td> ' . $row['faculty'] . '  </td>';
        echo '<td>' . $row['date'] . '</td>';
        echo "</tr>";

    }

}

function getReportFile($fileName)
{
    try {
        $DIR = __DIR__;
        $ext = explode("\"", $DIR);
        $DIR = "";
        for ($j = 0; $j < (count($ext) - 1); $j++) {
            $DIR .= $ext[$j] . "\"";
        }

        $ext = explode(".", $fileName);
        $fileName = "";
        for ($j = 0; $j < (count($ext) - 1); $j++) {
            $fileName .= $ext[$j];
        }


        $files = scandir(DIRECTORY_REPORT_STORE, 1);
        foreach ($files as $d) {
            $dOut = $d;
            $ext = explode(".", $d);
            $extension = $ext[(count($ext) - 1)];
            $d = "";
            for ($j = 0; $j < (count($ext) - 1); $j++) {
                $d .= $ext[$j];
            }

            if (strpos($d, $fileName) !== false) {
                return $dOut;
            }


        }
    } catch (Exception $e) {
        echo 'Caught exception: ', $e->getMessage(), "\n";
    }
    return "";
}


function userForm()
{
    echo '<form id="idForm" name="idForm"    >' .
        '<input type = "hidden" id= "action" name= "action" value="updateUser">' .
        '<input type = "hidden" id= "idUser" name= "idUser"  value="new">';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Login</label>   <div class="col-sm-9"> <input type = "text" id= "roNameProduct" name= "roNameProduct"  class="form-control" value=" "></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Parola</label>   <div class="col-sm-9"> <input type = "text" id= "ruNameProduct" name= "ruNameProduct"  class="form-control" value=" "></div></div>';
    echo ' </form>';
}


function updateUser($params)
{
    $out = " ";
    $idProduct = $params["idUser"];
    $roNameProduct = $params["roNameProduct"];
    $ruNameProduct = $params["ruNameProduct"];

    if (strcmp($idProduct, "new") == 0) {
        $stmt = DB::prepare("INSERT INTO `admintable` (`id`) VALUES (NULL)");
        $stmt->execute();
        $idProduct = DB::lastInsertId();
    }

    $idProduct = intval($idProduct);

    if (!isset($_SESSION)) {
        session_start();
    }
    $_SESSION['idUser'] = $idProduct;

    $stmt = DB::run("UPDATE  admintable SET  `login` =  ?,`pass` =   ?   WHERE  `id` = ?;",
        [$roNameProduct, $ruNameProduct, $idProduct]);
    if ($stmt->rowCount() == 1) {
        echo '            Success';
    }

    return $out;
}


function deleteUserForm($params)
{
    $id = intval($params['idUser']);
    $row = DB::run("DELETE  FROM admintable WHERE id=?", [$id]);
    return $row->rowCount();
}

function getAllUsers()
{
    $stmt = DB::run("SELECT * FROM admintable  ORDER BY  `admintable`.`id` DESC ");
    while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
        $check = "";

        if (strcmp($row['isAdmin'], '0') != 0) continue;// $check = "checked";
        echo ' <tr id="rows' . $row['id'] . '" >
                            <td>' . $row['name'] . '  </td>
                            <td>' . $row['surname'] . '  </td>
                            <td>' . $row['login'] . '</td>
                            <td>' . $row['pass'] . '</td>
                            <td>' . $row['faculty'] . '</td>
                            <td> <input class="checkbox disabled" disabled  type="checkbox" name="admin"  ' . $check . ' value="admin"></td>
                          <td>
                          <button id="edit' . $row['id'] . '"  onClick="edit(this)" class="btn btn-lg btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Editeaza"><div class="icon-hover"><i class="fa fa-server "></i>   Editeaza</div></button>
                          <button id="dele' . $row['id'] . '"  onClick="del(this)" class="btn btn-lg btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Sterge"><div class="icon-hover"><i class="fa fa-times"></i>  Sterge </div></button>

                           </td>
                        </tr>';
    }
}

function upload($params, $id, $destination)
{
    $target_dir = $destination;
    $target_file = $target_dir . basename($params[$id]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $check = getimagesize($params[$id]["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        $uploadOk = 0;
    }
    if (file_exists($target_file) || ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif")) {
        $uploadOk = 0;
    }
    if ($uploadOk != 0) {

        $ext = explode('.', $params[$id]["name"]);
        $extension = $ext[(count($ext) - 1)];
        $name = "";
        for ($i = 0; $i < (count($ext) - 1); $i++) {
            $name .= $ext[$i];
        }
        $newname = $name . '-' . time() . '-' . rand();
        $full_local_path = $destination . $newname . "." . $extension;
        if (move_uploaded_file($params[$id]["tmp_name"], $full_local_path)) {
            return $newname . "." . $extension;
        } else {
            return "";
        }
    }
    return "";
}


/*
 * Function for lever acces
 *
 */

function  isAuth()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    $login = $_SESSION['login'];
    $hash = $_SESSION['hash'];

    if (isset($login) && isset($hash) && !empty($login) && !empty($hash)) {
        $row = DB::run("SELECT  login FROM admintable WHERE pass=? AND login=?", [$hash, $login])->fetchColumn();
        if (strcmp($_SESSION['login'], $row) == 0) {
            return true;
        }
    }
    return false;
}

function  isAdmin()
{
    if (isAuth()) {

        $hash = $_SESSION['hash'];
        $login = $_SESSION['login'];
        $row = DB::run("SELECT  isAdmin FROM admintable WHERE pass=? AND login = ?", [$hash, $login])->fetchColumn();
        if ((strcmp("1", $row) == 0)) {
            return true;
        }
    }

    return false;
}







