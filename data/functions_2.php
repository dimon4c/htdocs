<?php
include("../variables.php");

if (isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch ($action) {

        case 'updateUser' :
            echo updateUser($_POST);

            break;

        case 'editUser' :
            getEditUserForm($_POST);
            break;

        case 'deleteUser' :
            echo deleteUserForm($_POST);
            break;

        case 'addUser' :
            userForm();
            break;

        default:
            echo "Some thing it is wrong";
            break;
    }
}

class DB
{
    protected static $instance = null;

    public function __construct()
    {
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function instance()
    {
        if (self::$instance === null) {
            $opt = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => TRUE,
            );
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '; ';
//            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHAR;
            self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
        }
        return self::$instance;
    }

    public static function run($sql, $args = [])
    {
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    public function __clone()
    {
    }
}


function getReportFile($fileName)
{
    $DIR = __DIR__;
    $ext = explode("\\", $DIR);
    $DIR = "";
    for ($j = 0; $j < (count($ext) - 1); $j++) {
        $DIR .= $ext[$j] . "\\";
    }

    $ext = explode(".", $fileName);
    $fileName = "";
    for ($j = 0; $j < (count($ext) - 1); $j++) {
        $fileName .= $ext[$j];
    }

    $files = scandir($DIR . DIRECTORY_REPORT_STORE, 1);

    foreach ($files as $d) {
        $dOut = $d;
        $ext = explode(".", $d);
        $extension = $ext[(count($ext) - 1)];
        $d = "";
        for ($j = 0; $j < (count($ext) - 1); $j++) {
            $d .= $ext[$j];
        }
        if (strpos($d, $fileName) !== false) {
            return $dOut;
        }


    }
    return "";
}

function userForm()
{
    echo '<form id="idForm" name="idForm"    >' .
        '<input type = "hidden" id= "action" name= "action" value="updateUser">' .
        '<input type = "hidden" id= "idUser" name= "idUser"  value="new">';

    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Name</label>   <div class="col-sm-9"> <input type = "text" id= "roName" name= "roName"  class="form-control" value=""></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Surname</label>   <div class="col-sm-9"> <input type = "text" id= "roSurname" name= "roSurname"  class="form-control" value=""></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Login</label>   <div class="col-sm-9"> <input type = "text" id= "roNameProduct" name= "roNameProduct"  class="form-control" value=""></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Parola</label>   <div class="col-sm-9"> <input type = "text" id= "ruNameProduct" name= "ruNameProduct"  class="form-control" value=""></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Facultatea</label>   <div class="col-sm-9"> <input type = "text" id= "enNameProduct" name= "enNameProduct"  class="form-control" value=""></div></div>';
    echo ' </form>';
}

function updateUser($params)
{
    $out = " ";
    $idProduct = $params["idUser"];
    $roName = $params["roName"];
    $roSurname = $params["roSurname"];
    $roNameProduct = $params["roNameProduct"];
    $ruNameProduct = $params["ruNameProduct"];
    $enNameProduct = $params["enNameProduct"];

    if (strcmp($idProduct, "new") == 0) {
        $stmt = DB::prepare("INSERT INTO `admintable` (`id`) VALUES (NULL)");
        $stmt->execute();
        $idProduct = DB::lastInsertId();
    }

    $idProduct = intval($idProduct);
    if (!isset($_SESSION)) {
        session_start();
    }
    $_SESSION['idUser'] = $idProduct;
    $stmt = DB::run("UPDATE   admintable SET  `login` =?,`pass` = ?, `faculty` =? , `name` =?, `surname` =?   WHERE  `id` = ?;",
        [$roNameProduct, $ruNameProduct, $enNameProduct, $roName, $roSurname, $idProduct]);
    if ($stmt->rowCount() == 1) {
        return '           Success';
    }
    return $out;
}

function getEditUserForm($params)
{
    $id = $params['idUser'];
    $row = DB::run("SELECT * FROM admintable WHERE id=?", [$id])->fetch();
    echo '<form id="idForm" name="idForm"     >' .
        '<input type = "hidden" id= "action" name= "action" value="updateUser">' .
        '<input type = "hidden" id= "idUser" name= "idUser"  value="' . $id . '">';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Name</label>   <div class="col-sm-9"> <input type = "text" id= "roName" name= "roName"  class="form-control" value="' . $row['name'] . '"></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Surname</label>   <div class="col-sm-9"> <input type = "text" id= "roSurname" name= "roSurname"  class="form-control" value="' . $row['surname'] . '"></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Login</label>   <div class="col-sm-9"> <input type = "text" id= "roNameProduct" name= "roNameProduct"  class="form-control" value="' . $row['login'] . '"></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Parola</label>   <div class="col-sm-9"> <input type = "text" id= "ruNameProduct" name= "ruNameProduct"  class="form-control" value="' . $row['pass'] . '"></div></div>';
    echo '<div class="form-group"> <label for="basicText" class="col-sm-3 control-label">Facultatea</label>   <div class="col-sm-9"> <input type = "text" id= "enNameProduct" name= "enNameProduct"  class="form-control" value="' . $row['faculty'] . '"></div></div>';
    echo '</form>';
}

function deleteUserForm($params)
{
    $id = intval($params['idUser']);
    $row = DB::run("DELETE  FROM admintable WHERE id=?", [$id]);
    return $row->rowCount() . $id;
}

function  isAuth()
{
    if (!isset($_SESSION)) {
        session_start();
    }
    $login = $_SESSION['login'];
    $hash = $_SESSION['hash'];

    if (isset($login) && isset($hash) && !empty($login) && !empty($hash)) {
        $row = DB::run("SELECT  login FROM admintable WHERE pass=? AND login=?", [$hash, $login])->fetchColumn();
        if (strcmp($_SESSION['login'], $row) == 0) {
            return true;
        }
    }
    return false;
}

function  isAdmin()
{
    if (isAuth()) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $hash = $_SESSION['hash'];
        $login = $_SESSION['login'];
        $row = DB::run("SELECT  isAdmin FROM admintable WHERE pass=? AND login =?", [$hash, $login])->fetchColumn();
        if ((strcmp("1", $row) == 0)) {
            return true;
        }
    }

    return false;
}





