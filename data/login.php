<?php
include_once("../variables.php");


class DB
{
    protected static $instance = null;

    public function __construct()
    {
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function instance()
    {
        if (self::$instance === null) {
            $opt = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => TRUE,
            );
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '; ';
//            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHAR;
            self::$instance = new PDO($dsn, DB_USER, DB_PASS, $opt);
        }
        return self::$instance;
    }

    public static function run($sql, $args = [])
    {
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    public function __clone()
    {
    }
}

try {

    $login = $_POST['login'];
    $pwd = $_POST['password'];
//$salt = "3Es@";
//$hash = hash('sha512', $pwd . $salt);
    $hash = $pwd;
    print_r(PDO::getAvailableDrivers());

    $row = DB::run("SELECT login FROM admintable WHERE pass=? and login=?", [$hash, $login])->fetchColumn();

    if (strcmp($login, $row) == 0) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $row = DB::run("SELECT id FROM admintable WHERE pass=? and login=?", [$hash, $login])->fetchColumn();

        $_SESSION['hash'] = $hash;
        $_SESSION['userId'] = $row;
        $_SESSION['login'] = $login;

        header('Location: ' . SITE_INDEX);
    } else {
        header('Location: ' . SITE_INDEX_LOGIN_PAGE);
    }
} catch (Exception $e) {
    echo 'Caught exception: ', $e->getMessage(), "\n";
}

