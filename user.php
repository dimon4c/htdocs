<?php
include_once('data/functions.php');

if (!isAuth()) {

    header('Location: ' . SITE_INDEX_LOGIN_PAGE);

    die();
} else {
}
if (!isAdmin()) {

}
include_once('includes/head.php');
include_once('includes/menu.php');
?>
<!--Main Content-->
<section class="l-container">
    <?php include_once('includes/topHeader.php') ?>
    <div class="l-page-header">
        <h2 class="l-page-title"><span>Utilizatori</span></h2>
        <!--BREADCRUMB-->
        <ul class="breadcrumb t-breadcrumb-page">
            <li><a href="index.php">Home</a></li
            <li></li>
            <li class="active">Utilizatori</li>
        </ul>

    </div>
    <div class="l-spaced">
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">
                    <button id="addd" onClick="add(this)" class="m-10 btn btn-primary btn-lg btn-eff btn-eff-2"
                            type="button"
                            data-toggle="tooltip" title="Add new" data-original-title="Add User"><i
                            class="fa fa-plus"></i></button>
                    <button id="open" onclick="openWindow()" class="m-10 btn btn-primary btn-lg btn-eff btn-eff-2"
                            type="button"
                            data-toggle="tooltip" title="Re-open window " data-original-title="Open"><i
                            class="fa fa-undo"></i></button>
                    <button id="refresh" onClick="location.reload();"
                            class="m-10 btn btn-primary btn-lg btn-eff btn-eff-2"
                            type="button"
                            data-toggle="tooltip" title="Refresh page " data-original-title="Refresh page"><i
                            class="fa fa-refresh"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="l-spaced">
        <div class="l-row">
            <div class="l-box">
                <div class="l-box-body">
                    <table id="dataTableId" class="table table-hover label=container" data-filter="#filter"
                           cellspacing="0" width="100%" class="display">
                        <thead>
                        <tr class="active">

                            <th data-hide="phone">Name</th>
                            <th data-toggle="true">Surname</th>
                            <th data-hide="phone">Login</th>
                            <th data-hide="phone">Password</th>
                            <th data-hide="phone">Faculty</th>
                            <th data-hide="phone">isAdmin</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tfoot class="hide-if-no-paging">
                        <tr>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Login</th>
                            <th>Password</th>
                            <th>Faculty</th>
                            <th>isAdmin</th>
                            <th></th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php include_once('data/functions.php');
                        include_once('variables.php');
                        getAllUsers(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<!--Modal Window -->

<div class="modal fade" id="successModal" operation="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title"> Window </h4>
            </div>
            <div id="modal-body" class="modal-body" idProduct="">
            </div>
            <div class="modal-footer">
                <button type="button" onclick="save()" class="btn btn-default"
                        data-dismiss="modal">Ok
                </button>
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">Inchide
                </button>
            </div>
        </div>
        <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
</div>
<!-- /.modal-->

<!--Modal Window -->

<!--Modal Window -->
<div class="modal  fade" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title"> Message </h4>
            </div>
            <div id="message-body" class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
        <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
</div>
<!-- /.modal-->

<!--Modal Window -->

<!-- ===== JS =====-->

<script type="text/javascript">
    function openWindow() {
        $('#successModal').modal('show');
    }


    function add() {

        document.getElementById('successModal').setAttribute('operation', 'add');
        document.getElementById('modal-body').setAttribute('idUser', "new");


        var st = '<form id="idForm">' +
            '<input type = "hidden" id= "action" name= "action" value="addUser">' +
            '<input type = "hidden"   name= "idUser"  value="new">' +

            '</form> ';

        $('.modal-body').html(st);

        $.ajax({
            url: 'data/functions_2.php',
            data: $("#idForm").serialize(),
            type: 'post',
            success: function (output) {
                $('.modal-body').html(output);
            }
        });
        $('#successModal').modal('show');

    }
    function edit(elmnt) {
        var str = elmnt.id;

        var id = str.substring(4, str.length);
        document.getElementById('successModal').setAttribute('operation', 'edit');
        document.getElementById('modal-body').setAttribute('idUser', id);


        var st = '<form id="idForm">' +
            '<input type = "hidden" id= "action" name= "action" value="editUser">' +
            '<input type = "hidden" id= "idCategories" name= "idUser"  value="' + id + '"></form> ';

        $('.modal-body').html(st);

        $.ajax({
            url: 'data/functions_2.php',
            data: $("#idForm").serialize(),
            type: 'post',
            success: function (output) {
                $('.modal-body').html(output);
            }
        });

        $('#successModal').modal('show');

    }

    function save() {
        var operation = document.getElementById('successModal').getAttribute('operation');
        document.getElementById('successModal').setAttribute('operation', 'close');

        switch (operation) {
            case  "add":
            case  "edit":
                $.ajax({
                    url: 'data/functions_2.php',
                    data: $("#idForm").serialize(),
                    type: 'post',
                    success: function (output) {
                        $('#message-body').html(output);
                    }
                });
                break;

            case  "delete":
                var out;
                $.ajax({
                    url: 'data/functions_2.php',
                    data: $("#idForm").serialize(),
                    type: 'post',
                    success: function (output) {
                        var id = output;
                        if (id == 1) {
                            out = "Categoria  a fost sters cu succes";
                            document.getElementById("row" + document.getElementById('modal-body').getAttribute('idUser')).innerHTML = "<td></td><td></td><td></td><td></td><td></td><td></td>";
                        }
                        $('#message-body').html(out);
                    }
                });
                $('#messageModal').modal('show');
                break;
            case  "close":
                break;

        }
    }


    function transferComplete(evt) {
        $('#messageModal').modal('show');
    }
    function del(elmnt) {
        var str = elmnt.id;
        var id = str.substring(4, str.length);

        document.getElementById('successModal').setAttribute('operation', 'delete');
        document.getElementById('modal-body').setAttribute('idUser', id);
        var st = '<form id="idForm">' +
            '<input type = "hidden" id= "action" name= "action" value="deleteUser">' +
            '<input type = "hidden"   id= "idUser"  name= "idUser"  value="' + id + '">' +

            '</form> ';

        $('.modal-body').html(st + "Doriti sa stergeti aceasta categorie cu id=" + id + " ? ");
        $('#successModal').modal('show');
    }
</script>


<!-- jQuery-->
<script src="js/basic/jquery.min.js"></script>
<script src="js/basic/jquery-migrate.min.js"></script>
<!-- General-->
<script src="js/basic/modernizr.min.js"></script>
<script src="js/basic/bootstrap.min.js"></script>
<script src="js/shared/jquery.asonWidget.js"></script>
<script src="js/plugins/plugins.js"></script>
<script src="js/general.js"></script>
<!-- Semi general-->
<script type="text/javascript">
    var paceSemiGeneral = {restartOnPushState: false};
    if (typeof paceSpecific != 'undefined') {
        var paceOptions = $.extend({}, paceSemiGeneral, paceSpecific);
        paceOptions = paceOptions;
    } else {
        paceOptions = paceSemiGeneral;
    }

</script>
<script src="js/plugins/pageprogressbar/pace.min.js"></script>
<!-- Specific-->
<script src="js/shared/classie.js"></script>
<script src="js/shared/perfect-scrollbar.min.js"></script>
<script src="js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
<script src="js/plugins/forms/elements/jquery.checkBo.min.js"></script>
<script src="js/plugins/forms/elements/jquery.switchery.min.js"></script>
<script src="js/plugins/table/jquery.dataTables.min.js"></script>
<script src="js/plugins/tooltip/jquery.tooltipster.min.js"></script>
<script src="js/calls/part.header.1.js"></script>
<script src="js/calls/part.sidebar.2.js"></script>
<script src="js/calls/part.theme.setting.js"></script>
<script src="js/calls/table.data.js"></script>
<script src="js/plugins/forms/elements/jquery.checkradios.min.js"></script>
<script src="js/plugins/forms/upload/jquery.ui.widget.js"></script>
<script src="js/plugins/forms/upload/jquery.tmpl.min.js"></script>
<script src="js/plugins/forms/upload/jquery.load-image.all.min.js"></script>
<script src="js/plugins/forms/upload/jquery.canvas-to-blob.min.js"></script>
<script src="js/plugins/forms/upload/jquery.blueimp-gallery.min.js"></script>
<script src="js/plugins/forms/upload/jquery.iframe-transport.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-process.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-image.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-audio.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-video.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-validate.js"></script>
<script src="js/plugins/forms/upload/jquery.fileupload-ui.js"></script>
<script src="js/plugins/forms/upload/jquery.summernote.min.js"></script>
<script src="js/plugins/forms/elements/jquery.switchery.min.js"></script>
<script src="js/plugins/tooltip/jquery.tooltipster.min.js"></script>
<script src="js/calls/form.upload.js"></script>
<script src="js/calls/table.foo.js"></script>
<script src="js/plugins/table/footable.all.min.js"></script>

</body>

</html>
